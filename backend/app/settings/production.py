from .base import *

PRODUCTION = True

DEBUG = True

ALLOWED_HOSTS = ['192.168.99.100', 'localhost', '127.0.0.1']

WSGI_APPLICATION = 'app.wsgi.application'

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': 'screening',
        'USER': 'postgres',
        'PASSWORD': 'eexbcZ7A2k3]ZcrW',
        'HOST': 'db.local',
        'PORT': '',
    }
}


STATIC_ROOT = '/var/static'
MEDIA_ROOT = '/var/media/pictures'
