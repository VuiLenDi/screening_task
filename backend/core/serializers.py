from rest_framework import serializers

from core.models import OfficeSpace


class OfficeSpaceSerializer(serializers.ModelSerializer):
    class Meta:
        model = OfficeSpace
        fields = ('name', 'street', 'city', 'zip_code', 'public', 'lat', 'lng')
