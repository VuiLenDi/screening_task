from rest_framework import viewsets

from core.models import OfficeSpace
from core.serializers import OfficeSpaceSerializer


class OfficeSpaceViewSet(viewsets.ModelViewSet):
    queryset = OfficeSpace.objects.all()
    serializer_class = OfficeSpaceSerializer
