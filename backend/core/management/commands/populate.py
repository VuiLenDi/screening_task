import os
from django.conf import settings

from decimal import Decimal
import random

from django.contrib.auth import get_user_model
from django.core.management import call_command, BaseCommand, CommandError

from core.constants import LANDLORD, TENANT
from core.models import CustomUser, OfficeSpace

User = get_user_model()


def office_space_factory(number):
    building_names = [
        'Central Tower', 'Centrum LIM', 'Generation Park', 'Intraco I',
        'Millennium Plaza', 'Oxford Tower', 'Palace of Culture and Science',
    ]

    number = number if number < len(building_names) else len(building_names)

    for i in range(number):
        yield {
            # Set in Model
            'name': building_names[i % len(building_names)],
            'street': 'Street 1',
            'city': 'City',
            'zip_code': '01-123',
            'lat': Decimal(random.randint(52, 53)),
            'lng': Decimal(random.randint(20, 21)),
        }


class Command(BaseCommand):
    """
    Populates local database for development with sample random
    objects.

    Never to be used in production.
    """

    def add_arguments(self, parser):
        parser.add_argument(
            '--office-spaces',
            type=int,
            default=10
        )

        # Create Custom User with some options
        parser.add_argument(
            '--create-user',
            action='store_true',
            default=False,
            help='Create user',
        )

        parser.add_argument(
            '--user-type',
            help='Type of user LANDLORD, TENANT',
        )

        parser.add_argument(
            '--email',
            help='Email of user',
        )

        parser.add_argument(
            '--is-super',
            help='Set user to superuser',
            default=False
        )

    def handle(self, *args, **options):
        if options['create_user']:
            if not options['user_type'] in [LANDLORD,TENANT]:
                raise CommandError('Please choose type of user --user-type [landlord, tenant]')
            if not options['email']:
                raise CommandError('Please choose email --email')
            try:
                CustomUser.objects.create(type=options['user_type'], email=options['email'], is_superuser=options['is_super'])
            except Exception as e:
                raise CommandError(repr(e))
        else:
            # If we do that how can we populate data ?
            # if os.environ['DJANGO_SETTINGS_MODULE'] == 'app.settings.production':
            #   raise CommandError('Cannot run in production')
            for office_space in office_space_factory(options['office_spaces']):
                print(office_space['name'])

                try:
                    OfficeSpace.objects.create(**office_space)
                except Exception:
                    raise CommandError('OfficeSpace "%s" already exist' % office_space['name'])
